#!/bin/bash

set -e

INCLUDE="-I/opt/raylib/src"
LIBS="-L/opt/raylib/src/ -lraylib -lm -ldl -lpthread"

gcc -o main main.c $INCLUDE $LIBS -ggdb
./main

