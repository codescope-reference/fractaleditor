#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>
#include "raylib.h"
#include "state.h"
#include "editor.c"


int screenshot_count = 0;
bool clean_ui = false;


State state = {
    .editor_open = false,
    .pattern = { 0 },
    .quitting = false,
};

Rectangle screen_rect = { 0 };


int get_state(Pattern pattern, int x, int y) {
    return pattern.pattern[x + y * pattern.width];
}


Image img;
Texture2D texture;

void draw_fractal(Pattern pattern, float start_x, float start_y, float width, float height, int levels, Color *state_to_color,
        int transform) {
    float square_width = width / pattern.width;
    float square_height = height / pattern.height;
    float size = fminf(square_width, square_height);
    square_width = square_height = size;
    if (levels <= 1) {
        for (int i = 0; i < pattern.width; i++) {
            float x = start_x + i * square_width;
            for (int j = 0; j < pattern.height; j++) {
                float y = start_y + j * square_height;
                int state = (get_state(pattern, i, j) + transform) % pattern.number_of_states;
                Color color = state_to_color[state];

                // DrawRectangleV((Vector2) { x, y }, (Vector2) { square_width, square_height }, color);
                if (j == pattern.height - 1) {
                    // ImageDrawRectangleV(&img, (Vector2) { x, y }, (Vector2) { square_width , square_height  }, color);
                    ImageDrawRectangleV(&img, (Vector2) { x, y }, (Vector2) { square_width + 5.0f, square_height + 5.0f }, color);
                } else {
                    ImageDrawRectangleV(&img, (Vector2) { x, y }, (Vector2) { square_width + 5.0f, square_height + 5.0f }, color);
                }
            }
        }
    } else {
        float aspect_ratio = (float)pattern.height / pattern.width;
        square_height *= aspect_ratio;
        for (int i = 0; i < pattern.width; i++) {
            float x = start_x + i * square_width;
            for (int j = 0; j < pattern.height; j++) {
                float y = start_y + j * square_height;
                int state = (get_state(pattern, i, j) + transform) % pattern.number_of_states;

                draw_fractal(pattern, x, y, square_width, square_height, levels - 1, state_to_color, state);
            }
        }
    }
}

void randomize_pattern(Pattern *pattern) {
    for (int i = 0; i < pattern->width; i++) {
        for (int j = 0; j < pattern->height; j++) {
            pattern->pattern[i + j * pattern->width] = GetRandomValue(0, pattern->number_of_states - 1);
            assert(pattern->pattern[i + j * pattern->width] < pattern->number_of_states);
        }
    }
}


int main(void) {
    screen_rect.x = 0;
    screen_rect.y = 0;
    screen_rect.width = 800;
    screen_rect.height = 800;
    img = GenImageColor(screen_rect.width, screen_rect.height, CLITERAL(Color) { 51, 0, 0, 255 });


    int *data = calloc(sizeof(int), 4);
    data[1] = 1;
    Color *state_to_color = malloc(sizeof(Color) * 2);
    state_to_color[0] = (Color) { 0, 0, 0, 255 };
    state_to_color[1] = (Color) { 255, 255, 255, 255 };

    Pattern pattern = {
        .pattern = data,
        .state_to_color = state_to_color,
        .width = 2,
        .height = 2,
        .number_of_states = 2
    };
    state.pattern = pattern;
    state.should_draw = true;


    // unsigned int configflags = FLAG_WINDOW_RESIZABLE;
    // SetConfigFlags(configflags);
    InitWindow(screen_rect.width, screen_rect.height, "Fractals");
    texture = LoadTextureFromImage(img);


    int levels = 3;
    bool should_quit = false;
    while (!should_quit) {
        BeginDrawing();
        ClearBackground( CLITERAL(Color) { 51, 51, 51, 255 });

        if (state.editor_open) {
            draw_editor(screen_rect, &state);
        } else  {
            int start_x;
            int start_y;
            int width;
            int height;
            if (clean_ui) {
                start_x = 0;
                start_y = 0;
                width  = screen_rect.width;
                height = screen_rect.height;
            } else {
                start_x = screen_rect.width / 8;
                start_y = screen_rect.height / 8;
                width  = screen_rect.width * 3 / 4;
                height = screen_rect.height * 3 / 4;
            }
            if (state.should_draw) {
                // draw_fractal(state.pattern, start_x, start_y, width, height, levels, state.pattern.state_to_color, 0);
                ImageClearBackground(&img, CLITERAL(Color) { 51, 51, 51, 255 });
                draw_fractal(state.pattern, 0, 0, screen_rect.width, screen_rect.height, levels, state.pattern.state_to_color, 0);
                state.should_draw = false;
                UpdateTexture(texture, img.data);
            } 
            Rectangle source = { 0, 0, screen_rect.width, screen_rect.height };
            Rectangle target = { start_x, start_y, width, height};
            BeginScissorMode(start_x, start_y, width, height);
            DrawTexturePro(texture, source, target, (Vector2){0, 0}, 0, WHITE);
            EndScissorMode();

            if (!clean_ui) {
                DrawText(TextFormat("Levels: %d", levels), 10, 10, 20, CLITERAL(Color) { 255, 255, 255, 255 });
            }
        } 
        EndDrawing();


        if (IsKeyReleased(KEY_U)) {
            levels++;
            state.should_draw = true;
        }
        if (IsKeyReleased(KEY_D)) {
            if (levels > 1) {
                levels--;
                state.should_draw = true;
            }
        }
        if (IsKeyReleased(KEY_E)) {
            state.editor_open = !state.editor_open;
        }
        if (IsKeyReleased(KEY_C)) {
            clean_ui = !clean_ui;
            state.should_draw = true;
        }
        if (IsKeyReleased(KEY_P)) {
            TakeScreenshot(TextFormat("screenshot_%d.png", screenshot_count++));
        }
        if (IsKeyReleased(KEY_M)) {
            randomize_pattern(&state.pattern);
            state.should_draw = true;
        }
        // if (IsWindowResized()) {
            // screen_rect.width = GetScreenWidth();
            // screen_rect.height = GetScreenHeight();
            // ImageResize(&img, screen_rect.width, screen_rect.height);
            // UnloadTexture(texture);
            // texture = LoadTextureFromImage(img);
            // state.should_draw = true;
        // }


        if (state.quitting) {
            should_quit = true;
        }
        if (WindowShouldClose()) {
            state.quitting = true;
        }
    }

    deinit_editor(screen_rect, &state);


    CloseWindow();

    free(state.pattern.pattern);
    free(state.pattern.state_to_color);

    return 0;
}
