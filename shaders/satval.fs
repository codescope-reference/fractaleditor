#version 330

in vec2 fragTexCoord;
in vec4 fragColor;

out vec4 finalColor;


// uniform vec3 color;
// uniform float sat;
uniform float hue;


vec3 hsvtorgb(float hue, float sat, float val) {
    float hp = hue / 360.0f;
    float c = val * sat;
    float x = c * (1.0f - abs(mod(hp * 6.0f, 2.0f) - 1.0f));
    float m = val - c;
    vec3 color;


    float r1 = 0.0f;
    float g1 = 0.0f;
    float b1 = 0.0f;

    if (hp < 1.0f / 6.0f) {
        r1 = c;
        g1 = x;
        b1 = 0.0f;
    } else if (hp < 2.0f / 6.0f) {
        r1 = x;
        g1 = c;
        b1 = 0.0f;
    } else if (hp < 3.0f / 6.0f) {
        r1 = 0.0f;
        g1 = c;
        b1 = x;
    } else if (hp < 4.0f / 6.0f) {
        r1 = 0.0f;
        g1 = x;
        b1 = c;
    } else if (hp < 5.0f / 6.0f) {
        r1 = x;
        g1 = 0.0f;
        b1 = c;
    } else {
        r1 = c;
        g1 = 0.0f;
        b1 = x;
    }

    color.r = r1 + m;
    color.g = g1 + m;
    color.b = b1 + m;

    return color;
}


void main() {
    finalColor = vec4(hsvtorgb(hue, fragTexCoord.x, 1.0f - fragTexCoord.y), 1.0);
    // finalColor = vec4(sat, 0.8, 0.8, fragColor.a);
}
