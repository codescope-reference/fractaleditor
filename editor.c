#include "raylib.h"
#include "state.h"
#include <stdbool.h>
#include <assert.h>


bool *toggles;
int *toggled_colors;
bool __editor_initialized = false;
int num_squares_x = 20;
int num_squares_y;
int square_size;


Color *colors;
bool *enabled_states;
int num_states = 10;

int get_number_of_active_states(void) {
    int count = 0;
    for (int i = 0; i < num_states; i++) {
        if (enabled_states[i]) {
            count++;
        }
    }

    return count;
}

Color get_nth_color(int index) {
    int num_active_state = get_number_of_active_states();
    if (num_active_state == 0) {
        return (Color){ 0, 0, 0, 255 };
    } else {
        int active_index = 0;
        for (int i = 0; i < num_states; i++) {
            if (!enabled_states[i]) {
                continue;
            }
            
            if (active_index == index) {
                return colors[i];
            } else {
                active_index++;
            }
        }
    }

    assert(0 && "Unreachable");
}


bool check_has_background(Rectangle bbox) {
    // Loop through all squares in the bounding box check if any are not toggled
    for (int i = 0; i < bbox.width; i++) {
        for (int j = 0; j < bbox.height; j++) {
            int x = bbox.x + i;
            int y = bbox.y + j;
            int index = x + y * num_squares_x;

            if (!toggles[index]) {
                return true;
            }
        }
    }

    return false;

}


Rectangle pattern_rect = { 0 };
Rectangle color_picker_rect = { 0 };
Rectangle states_rect = { 0 };

float hue = 0; // 0...360
float sat = 0.8;
float val = 0.8;
Color color = { 0, 0, 0, 255 };
bool dragging_picker = false;


Shader shader;
RenderTexture2D render_target;

void init_editor(Rectangle screen, State *state) {
    printf("Initializing with screen: %f, %f\n", screen.width, screen.height);

    // Initialize rectangle for making a pattern
    // // Initialize rectangle for making a patter
    float pattern_width = 0.4 * screen.width;
    pattern_rect.x = screen.x;
    pattern_rect.y = screen.y + screen.height / 2 - pattern_width / 2;
    pattern_rect.width = pattern_width;
    pattern_rect.height = pattern_width;
    square_size = pattern_rect.width / num_squares_x;
    num_squares_y = pattern_rect.height / square_size;

    // Initialize state selector widget
    float states_width = 0.3 * screen.width;
    states_rect.x = screen.x + pattern_rect.width;
    states_rect.y = pattern_rect.y;
    states_rect.width = states_width;
    states_rect.height = states_width;
    colors = malloc(sizeof(Color) * num_states);
    enabled_states = calloc(sizeof(bool), num_states);



    // Initialize rectangle for color picker
    float color_picker_width = 0.3 * screen.width;
    color_picker_rect.x = states_rect.x + states_rect.width;
    color_picker_rect.y = states_rect.y;
    color_picker_rect.width = color_picker_width;
    color_picker_rect.height = color_picker_width;


    __editor_initialized = true;
    toggles = calloc(sizeof(bool), num_squares_x * num_squares_y);
    toggled_colors = malloc(sizeof(int) * num_squares_x * num_squares_y);


    // Shader init
    shader = LoadShader(0, "shaders/satval.fs");
    render_target = LoadRenderTexture(screen.width, screen.height);
}

void deinit_editor(Rectangle screen, State *state) {
    free(toggles);
    free(toggled_colors);
    free(colors);
    free(enabled_states);   
    UnloadShader(shader);
    UnloadRenderTexture(render_target);
}


Rectangle toggles_bounding_box(void) {
    int min_x = num_squares_x;
    int min_y = num_squares_y;
    int max_x = 0;
    int max_y = 0;
    bool any_on = false;
    for (int i = 0; i < num_squares_x; i++) {
        for (int j = 0; j < num_squares_y; j++) {
            if (toggles[i + j * num_squares_x]) {
                if (i < min_x) {
                    min_x = i;
                }
                if (i > max_x) {
                    max_x = i;
                }
                if (j < min_y) {
                    min_y = j;
                }
                if (j > max_y) {
                    max_y = j;
                }

                any_on = true;
            }
        }
    }

    if (!any_on) {
        return (Rectangle) { 0, 0, 0, 0 };
    }


    return (Rectangle) { min_x, min_y, max_x - min_x + 1, max_y - min_y + 1 };
}

bool draw_clickable_square(int x, int y, int square_size, int index, Color outline_color) {
    Color color;
    if (toggles[index]) {
        if (get_number_of_active_states() == 0) {
            color = BLACK;
        } else {
            int color_index = toggled_colors[index] % get_number_of_active_states();
            color = get_nth_color(color_index);
        }
    } else {
        color = WHITE;
    }

    DrawRectangle(x, y, square_size, square_size, color);
    DrawRectangleLines(x, y, square_size, square_size, outline_color);


    Rectangle rect = { x, y, square_size, square_size };
    Vector2 mouse_pos = GetMousePosition();
    if (IsMouseButtonReleased(MOUSE_BUTTON_LEFT) && CheckCollisionPointRec(mouse_pos, rect)) {
        if (!toggles[index]) {
            toggled_colors[index] = 0;
        } else {
            if (get_number_of_active_states() == 0) {
                toggled_colors[index] = 0;
            } else {
                toggled_colors[index] = (toggled_colors[index] + 1) % get_number_of_active_states();
            }
        }

        toggles[index] = true;
        return true;
    }

    if (IsKeyDown(KEY_T) && CheckCollisionPointRec(mouse_pos, rect)) {
        toggles[index] = true;
        toggled_colors[index] = 0;
        return true;
    }

    if (IsKeyDown(KEY_D) && CheckCollisionPointRec(mouse_pos, rect)) {
        toggles[index] = false;
        return true;
    }

    if (IsMouseButtonReleased(MOUSE_BUTTON_RIGHT) && CheckCollisionPointRec(mouse_pos, rect)) {
        toggles[index] = false;
    }

    return false;
}


void draw_pattern(void) {
    for (int i = 0; i < num_squares_x; i++) {
        int x = i * square_size + pattern_rect.x;
        for (int j = 0; j < num_squares_y; j++) {
            int y = j * square_size + pattern_rect.y;
            draw_clickable_square(x,
                                  y,
                                  square_size,
                                  j * num_squares_x + i,
                                  CLITERAL(Color) { 51, 51, 51, 255 });
        }
    }
}


void draw_states(void) {
    // DrawRectangleRec(states_rect, CLITERAL(Color) { 255, 0, 0, 255 });

    int state_size = states_rect.width / num_states;
    for (int i = 0; i < num_states; i++) {
        int x = i * state_size + states_rect.x;
        int y = states_rect.y + states_rect.height / 2;
        Rectangle rect = { x, y, state_size, state_size };
        if (enabled_states[i]) {
            DrawRectangleRec(rect, colors[i]);
        } else {
            DrawRectangleRec(rect, CLITERAL(Color) { 255, 255, 255, 255 });
        }

        Vector2 mouse_pos = GetMousePosition();
        if (CheckCollisionPointRec(mouse_pos, rect) && IsMouseButtonReleased(MOUSE_BUTTON_LEFT)) {
            if (!enabled_states[i]) {
                enabled_states[i] = true;
                colors[i] = color;
            } else {
                enabled_states[i] = false;
            }
        }

        DrawRectangleLines(x, y, state_size, state_size, CLITERAL(Color) { 51, 51, 51, 255 });
    }
}

bool dragging_slider = false;
void draw_slider(Rectangle rect, int start, int end, float *target) {
    DrawRectangleRec(rect, CLITERAL(Color) { 255, 255, 255, 255 });

    int current_position = (float)*target / (end - start) * rect.width + rect.x;
    int circle_height = rect.y + rect.height / 2;
    float circle_radius = rect.height * 0.8f;
    Color circle_color = CLITERAL(Color) { 51, 51, 51, 255 };

    DrawCircle(current_position, circle_height, circle_radius, circle_color);
    Vector2 circle_center = { current_position, circle_height };
    Vector2 mouse_pos = GetMousePosition();
    if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT) 
            && CheckCollisionPointRec(mouse_pos, rect)) {
        dragging_slider = true;
    }

    if (dragging_slider) {
        int new_position = mouse_pos.x;
        if (new_position < rect.x) {
            new_position = rect.x;
        }
        if (new_position > rect.x + rect.width) {
            new_position = rect.x + rect.width;
        }

        *target = (float)(new_position - rect.x) / rect.width * (end - start);
    }

    if (dragging_slider && IsMouseButtonReleased(MOUSE_BUTTON_LEFT)) {
        dragging_slider = false;
    }
}


typedef void (*draw_func)(void);
void switch_hue(void) {
    hue = (hue + 180.0f);
    if (hue > 360.0f) {
        hue -= 360.0f;
    }
}

void randomize_color(void) {
    hue += GetRandomValue(-5, 5);
    int amp = 5;
    sat += (float)GetRandomValue(-amp, amp) / 100.0f;
    val += (float)GetRandomValue(-amp, amp) / 100.0f;

    if (hue > 360.0f) {
        hue -= 360.0f;
    }
    if (hue < 0.0f) {
        hue += 360.0f;
    }


    if (sat > 1.0f) {
        sat = 1.0f;
    }

    if (sat < 0.0f) {
        sat = 0.0f;
    }

    if (val > 1.0f) {
        val = 1.0f;
    }

    if (val < 0.0f) {
        val = 0.0f;
    }

}


void draw_button(Rectangle location, const char *text, draw_func action) {
    int font_size = 20;
    int text_len = MeasureText(text, font_size);
    while (text_len > location.width && font_size > 0) {
        font_size--;
        text_len = MeasureText(text, font_size);
    }
    location.height = font_size;
    Vector2 mouse_pos = GetMousePosition();
    // int text_height = MeasureText(test, 10
    if (CheckCollisionPointRec(mouse_pos, location)) {
        DrawRectangleRec(location, CLITERAL(Color) { 255, 255, 255, 255 });
        if (IsMouseButtonReleased(MOUSE_BUTTON_LEFT)) {
            action();
        }
    } else {
        int gray_level = 200;
        DrawRectangleRec(location, CLITERAL(Color) { gray_level, gray_level, gray_level, 255 });
    }
    // DrawRectangleLinesEx(location, 2.0, CLITERAL(Color) { 0, 0, 0, 255 });

    DrawText(text, location.x, location.y, font_size, CLITERAL(Color) { 51, 51, 51, 255 });
}

void draw_color_picker(void) {
    // Background
    DrawRectangleRec(color_picker_rect, CLITERAL(Color) { 100, 100, 100, 255 });


    // Setup and run shader
    int hueLoc = GetShaderLocation(shader, "hue");
    SetShaderValue(shader, hueLoc, &hue, SHADER_UNIFORM_FLOAT);
    float scale_factor = 0.8f;
    float scale = color_picker_rect.width * scale_factor / render_target.texture.width;
    BeginShaderMode(shader);
        DrawTextureEx(render_target.texture, (Vector2) { color_picker_rect.x, color_picker_rect.y }, 0, scale, WHITE);
    EndShaderMode();


    // Draw picker circle
    float pick_radius = color_picker_rect.width * 0.03;
    Vector2 mouse_pos = GetMousePosition();
    Vector3 circle = { color_picker_rect.x + sat * color_picker_rect.width * scale_factor,
                       color_picker_rect.y + (1.0f - val) * color_picker_rect.height * scale_factor,
                        pick_radius };
    DrawCircle(circle.x, circle.y, circle.z, WHITE);
    Rectangle picker_rect = { color_picker_rect.x, color_picker_rect.y, color_picker_rect.width * scale_factor, color_picker_rect.height * scale_factor};
    if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && CheckCollisionPointRec(mouse_pos, picker_rect)) { 
        dragging_picker = true;
    }

    if (dragging_picker) {
        float new_x = mouse_pos.x;
        float new_y = mouse_pos.y;
        if (new_x < color_picker_rect.x) {
            new_x = color_picker_rect.x;
        }
        if (new_x > color_picker_rect.x + color_picker_rect.width * scale_factor) {
            new_x = color_picker_rect.x + color_picker_rect.width * scale_factor;
        }
        if (new_y < color_picker_rect.y) {
            new_y = color_picker_rect.y;
        }
        if (new_y > color_picker_rect.y + color_picker_rect.height * scale_factor) {
            new_y = color_picker_rect.y + color_picker_rect.height * scale_factor;
        }

        sat = (new_x - color_picker_rect.x) / (color_picker_rect.width * scale_factor);
        val = 1.0f - (new_y - color_picker_rect.y) / (color_picker_rect.height * scale_factor);
    }

    if (dragging_picker && IsMouseButtonReleased(MOUSE_BUTTON_LEFT)) {
        dragging_picker = false;
    }


    // Make a rectangle showing the current color
    Rectangle show_color_rect = { color_picker_rect.x + color_picker_rect.width *scale_factor + 10,
                                  color_picker_rect.y,
                                  50, 50 };
    DrawRectangleRec(show_color_rect, color);



    // Slider
    int slider_width = 0.5 * color_picker_rect.width;
    int slider_x = color_picker_rect.x + color_picker_rect.width / 2 - 10;
    int slider_height = 10;
    int margin = 10;
    int slider_y = color_picker_rect.y + color_picker_rect.height - slider_height - margin;
    Rectangle slider_rect = { slider_x, slider_y, slider_width, slider_height };
    draw_slider(slider_rect, 0, 360, &hue);

    // Make button to the left of slider to switch hue -> hue + 180 % 360
    Rectangle switch_hue_rect = { color_picker_rect.x + 10, slider_y - 20, 50, 10 };
    draw_button(switch_hue_rect, "Switch", &switch_hue);

    Rectangle randomize_hue_rect = { color_picker_rect.x + 10, slider_y, 50, 10 };
    draw_button(randomize_hue_rect, "Rand", &randomize_color);




    color = ColorFromHSV(hue, sat, val);
}


void draw_editor(Rectangle screen, State *state) {
    if (!__editor_initialized) {
        printf("Initializing editor\n");
        init_editor(screen, state);
    }

    state->should_draw = true; // Always draw upon exiting editor


    // Drawing
    DrawText("Fractal Editor", screen.x + 10, screen.y + 10, 20, CLITERAL(Color) { 255, 255, 255, 255 });
    draw_pattern();
    draw_states();
    draw_color_picker();

    // Input handling
    if (IsKeyReleased(KEY_S)) {
        // Find bounding box of toggles squares
        Rectangle bbox = toggles_bounding_box();

        // Then make a state->pattern out of it
        int new_width = bbox.width;
        int new_height = bbox.height;

        if (new_width != state->pattern.width || new_height != state->pattern.height) {
            free(state->pattern.pattern);
            state->pattern.pattern = calloc(sizeof(int), new_width * new_height);
            state->pattern.width = new_width;
            state->pattern.height = new_height;
        }

        int num_states = get_number_of_active_states();
        if (num_states > 0) {
            bool has_background = check_has_background(bbox);
            if (has_background) {
                num_states = num_states + 1; // Add one for the background color
            }

            if (num_states != state->pattern.number_of_states) {
                free(state->pattern.state_to_color);
                state->pattern.state_to_color = calloc(sizeof(Color), num_states);
                state->pattern.number_of_states = num_states;
            }

            int state_index = 0;
            int limit = has_background ? num_states - 1 : num_states;
            for (int i = 0; i < limit; i++) {
                Color color = get_nth_color(i);
                state->pattern.state_to_color[i] = color;
            }

            if (has_background) {
                state->pattern.state_to_color[num_states - 1] = WHITE; // "Background"
            }
        } else {
            // No colors, reset state_to_color to show black and white
            if (state->pattern.number_of_states != 2) {
                free(state->pattern.state_to_color);
                state->pattern.state_to_color = calloc(sizeof(Color), 2);
                state->pattern.number_of_states = 2;
            }

            state->pattern.state_to_color[0] = BLACK;
            state->pattern.state_to_color[1] = WHITE;

        }

        for (int j = 0; j < new_height; j++) {
            for (int i = 0; i < new_width; i++) {
                int x = bbox.x + i;
                int y = bbox.y + j;
                int index = x + y * num_squares_x;

                if (num_states > 0) {
                    state->pattern.pattern[i + j * new_width] = toggles[index] ? toggled_colors[index] : num_states - 1;
                } else {
                    state->pattern.pattern[i + j * new_width] = !toggles[index];
                }
            }
        }
    }



    if (IsKeyReleased(KEY_R)) {
        memset(toggles, 0, sizeof(bool) * num_squares_x * num_squares_y);
    }


    if (__editor_initialized && state->quitting) {
        // deinit_editor(screen, state);
    }
}
