#ifndef STATE_H
#define STATE_H
#include <stdbool.h>
#include "raylib.h"


typedef struct {
    int *pattern;
    Color *state_to_color;
    int width;
    int height;
    int number_of_states;
} Pattern;


typedef struct State {
    bool editor_open;
    Pattern pattern;
    bool quitting;
    bool should_draw;
} State;

#endif // STATE_H
